@ECHO OFF&(PUSHD "%~DP0")&(REG QUERY "HKU\S-1-5-19">NUL 2>&1)||(
powershell -Command "Start-Process '%~sdpnx0' -Verb RunAs"&&EXIT)

taskkill /f /im KwW* >NUL 2>NUL
taskkill /f /im KwMusic* >NUL 2>NUL
taskkill /f /im KwWebKit*  >NUL 2>NUL
taskkill /f /im KwService* >NUL 2>NUL
taskkill /f /im runshelldraw* >NUL 2>NUL
taskkill /f /im WriteMbox.exe >NUL 2>NUL
taskkill /f /im KwKnowSong.exe >NUL 2>NUL

rd/s/q "%temp%\KWMUSIC" 2>NUL
del/f/s/q "%temp%\kuwo*" >NUL 2>NUL
del/f/s/q "%temp%\KwBindApp*" >NUL 2>NUL
reg delete "HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /f /v "kwmusic" >NUL 2>NUL
reg delete "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /f /v "kwmusic" >NUL 2>NUL
reg delete "HKLM\SOFTWARE\\Microsoft\Windows\CurrentVersion\Run" /f /v "kwmusic" /reg:32 >NUL 2>NUL

::允许防火墙入站规则
netsh advfirewall firewall add rule name="酷我核心服务" dir=in action=allow program="%~dp0bin\KwService.exe" >NUL 2>NUL 
netsh advfirewall firewall add rule name="酷我音乐" dir=in action=allow program="%~dp0bin\kwmusic.exe" >NUL 2>NUL

ver|findstr "5\.[0-9]\.[0-9][0-9]*" >NUL && (
rd /s/q "%AllUsersProfile%\Application Data\kuwodata\kwshow" 2>NUL
rd /s/q "%AllUsersProfile%\Application Data\kuwodata\kwmusic2013\Update" 2>NUL
del/f/q "%AllUsersProfile%\Application Data\kuwodata\kwmusic2013\ModuleData\ModMusicTool\conf.txt" >NUL 2>NUL
md "%AllUsersProfile%\Application Data\kuwodata\kwmusic2013\ModuleData\ModMusicTool" 2>NUL
echo f|copy /y "Bin\conf.txt" "%AllUsersProfile%\Application Data\kuwodata\kwmusic2013\ModuleData\ModMusicTool" >NUL 2>NUL
attrib +r "%AllUsersProfile%\Application Data\kuwodata\kwmusic2013\ModuleData\ModMusicTool\conf.txt" 2>NUL
echo. >"%AllUsersProfile%\Application Data\kuwodata\kwmusic2013\Update" 2>NUL
echo. >"%AllUsersProfile%\Application Data\kuwodata\kwshow" 2>NUL
)

ver|findstr "\<6\.[0-9]\.[0-9][0-9]*\> \<10\.[0-9]\.[0-9][0-9]*\>" >NUL && (
rd /s/q "%ProgramData%\kuwodata\kwshow" 2>NUL
rd /s/q "%ProgramData%\kuwodata\kwmusic2013\Update" 2>NUL
del/f/q "%ProgramData%\kuwodata\kwmusic2013\ModuleData\ModMusicTool\conf.txt" >NUL 2>NUL
md "%ProgramData%\Kuwodata\kwmusic2013\ModuleData\ModMusicTool" 2>NUL
echo f|copy /y "Bin\conf.txt" "%ProgramData%\Kuwodata\kwmusic2013\ModuleData\ModMusicTool" >NUL 2>NUL
echo. >"%ProgramData%\kuwodata\kwmusic2013\Update" 2>NUL
echo. >"%ProgramData%\kuwodata\kwshow" 2>NUL
)

ECHO.&ECHO 尽情享用吧！
ECHO.&ECHO.是否创建酷我音乐桌面快捷方式？
ECHO.&ECHO.是请按任意键，否请直接关闭窗口 &PAUSE >NUL 2>NUL

mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\酷我音乐.lnk""):b.TargetPath=""%~sdp0KwMusic.exe"":b.WorkingDirectory=""%~sdp0"":b.Save:close")

CLS & ECHO.&ECHO 创建完成，按任意键退出 &PAUSE >NUL 2>NUL
