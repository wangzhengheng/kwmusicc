// 适配
var $skinConfig = {};
$(function() {
  $skinConfig = JSON.parse(window.parent.document.getElementById('banner_iframe').getAttribute('data-config'));
});
$(window).resize(function() {
  setFocus();
  setIframeHeight();
});

// 全局存储已发送日志 sourceId
var logSourceIds = []

function callMainDataFn(jsondata) { // 创建完成后发送ServiceLevel日志
  var bannerArr = jsondata.data.focusPicture.list.reverse();
  createIndexFocusList(bannerArr);
  initJDT();
}

// 焦点图相关开始
// 创建焦点图
var focusClassArr;
// 大焦点图点击参数
var bigBannerClickObj = {};

function createIndexFocusList(data) {
  focusClassArr = [];
  // 焦点图图片list模板
  var listModel = loadTemplate('#kw_bannerlist .m_bannerImg');
  var item = '';
  if (!data) {
    data = [{
      inFo: '21首歌曲',
      pic2017: 'http://img4.kwcdn.kuwo.cn/star/upload/1/1/1521473597921_.jpg',
      nodeId: 0,
      source: 8,
      disName: '华语l纪念那些曾经的经典(一)',
      extend: '|MUSIC_COUNT=0|LABEL_TXT=高音质|LABEL_COLOR=#4c9de8|LABEL_SHOW=1||AL_FLAG=1',
      isNew: 0,
      name: '华语l纪念那些曾经的经典(一)',
      pic: 'http://img1.kwcdn.kuwo.cn/star/upload/6/6/1521473579254_.jpg',
      newCount: 0,
      sourceId: '2377302827'
    }, {
      inFo: '',
      pic2017: 'http://img1.kwcdn.kuwo.cn/star/upload/6/6/1520993477526_.jpg',
      nodeId: 0,
      source: 17,
      disName: 'Mike Angelo《你在我心中》',
      extend: '|MUSIC_COUNT=0|LABEL_TXT=付费专辑|LABEL_COLOR=#4c9de8|LABEL_SHOW=1|',
      isNew: 0,
      name: 'Mike Angelo《你在我心中》',
      pic: 'http://img4.kwcdn.kuwo.cn/star/upload/10/10/1520993462106_.jpg',
      newCount: 0,
      sourceId: 'http://vip1.kuwo.cn/fans/fans/template/index.html?key=mike20180013&fromSrc=18'
    }, {
      inFo: '181首歌曲',
      pic2017: 'http://img1.kwcdn.kuwo.cn/star/upload/11/11/1521438909755_.jpg',
      nodeId: 0,
      source: 8,
      disName: '《声临其境》',
      extend: '|MUSIC_COUNT=0|LABEL_TXT=综艺|LABEL_COLOR=#ff6749|LABEL_SHOW=1|',
      isNew: 0,
      name: '《声临其境》',
      pic: 'http://img1.kwcdn.kuwo.cn/star/upload/3/3/1521438897715_.jpg',
      newCount: 0,
      sourceId: '2427671621'
    }, {
      inFo: '表哥[主播]',
      pic2017: 'http://img1.kwcdn.kuwo.cn/star/upload/13/13/1521472798109_.jpg',
      nodeId: 0,
      source: 13,
      disName: '表哥诡话',
      extend: '|MUSIC_COUNT=0|LABEL_TXT=酷我调频|LABEL_COLOR=#ff6749|LABEL_SHOW=1|',
      isNew: 0,
      name: '表哥诡话',
      pic: 'http://img3.kwcdn.kuwo.cn/star/upload/8/8/1521472766249_.jpg',
      newCount: 0,
      sourceId: '5303780'
    }, {
      inFo: '麦冬',
      pic2017: 'http://img4.kwcdn.kuwo.cn/star/upload/7/7/1521473071527_.jpg',
      nodeId: 0,
      source: 13,
      disName: '职场情报局',
      extend: '|AL_FLAG=1|MUSIC_COUNT=0|LABEL_TXT=酷我调频|LABEL_COLOR=#4c9de8|LABEL_SHOW=1|',
      isNew: 0,
      name: '职场情报局',
      pic: 'http://img1.kwcdn.kuwo.cn/star/upload/6/6/1521473049398_.jpg',
      newCount: 0,
      sourceId: '4965270'
    }, {
      inFo: '林志颖',
      pic2017: 'http://img4.kwcdn.kuwo.cn/star/upload/3/3/1521390041155_.jpg',
      nodeId: 0,
      source: 13,
      disName: '我的骄傲',
      extend: '|AL_FLAG=1|MUSIC_COUNT=0|LABEL_TXT=独家首发|LABEL_COLOR=#ff6749|LABEL_SHOW=1|',
      isNew: 0,
      name: '我的骄傲',
      pic: 'http://img1.kwcdn.kuwo.cn/star/upload/11/11/1521390023195_.jpg',
      newCount: 0,
      sourceId: '5409679'
    }, {
      inFo: '',
      pic2017: 'http://img1.kwcdn.kuwo.cn/star/upload/7/7/1516187344695_.jpg',
      nodeId: 0,
      source: 17,
      disName: '酷我秀场 真情点唱',
      extend: '|XIUOPEN|MUSIC_COUNT=0|LABEL_TXT=酷我聚星|LABEL_COLOR=#ff6749|LABEL_SHOW=1|',
      isNew: 0,
      name: '酷我秀场 真情点唱',
      pic: 'http://img4.kwcdn.kuwo.cn/star/upload/2/2/1462442656146_.jpg',
      newCount: 0,
      sourceId: 'http://jx.kuwo.cn/KuwoLive/OpenLiveRoomLinkForKw?from=1001003001'
    }, {
      inFo: '',
      pic2017: 'http://img2.kwcdn.kuwo.cn/star/upload/7/7/1521464315335_.jpg',
      nodeId: 0,
      source: 17,
      disName: '酷我无线耳机',
      extend: '|MUSIC_COUNT=0|LABEL_TXT=酷我出品|LABEL_COLOR=#ff6749|LABEL_SHOW=1|',
      isNew: 0,
      name: '酷我无线耳机',
      pic: 'http://img4.kwcdn.kuwo.cn/star/upload/0/0/1521464304640_.jpg',
      newCount: 0,
      sourceId: 'http://c.kuwo.cn/g.real?aid=text_ad_3036&cid=&url=https://detail.tmall.com/item.htm?spm=a230r.1.14.6.NP89RL&id=522001391451&sku_properties=5919063:6536025'
    }];
  }
  var count = 0;
  var len = data.length;
  var btnHtml = '';
  while (count < len) {
    if (sourceISOK(data[count].source)) {
      data.splice(count, 1);
    } else {
      data[count].indexnum = count;
      count++;
    }
    if (count === len - 1) {
      item = 'focusList_2';
    } else if (count === len) {
      item = 'focusList_1';
    } else if (count === 1) {
      item = 'focusList_last';
    } else {
      item = '';
    }
    data[count - 1].itemClassName = item;
    focusClassArr.push(item);
    var classname = '';
    if (count === 1) {
      classname = 'current';
    }
    btnHtml += '<a href="javascript:;" class="' + classname + '" hidefocus=""></a>';
  }
  focusindex = 0;
  var listHtml = drawListTemplate(data, listModel, proIndexFocusData);
  $('#w_focus .focusList').html(listHtml);
  $('#focus_btn').html(btnHtml);
  setTimeout(function() {
    loadImagesNew();
  }, 500);
  onSkinChange($skinConfig);
}

function proIndexFocusData(obj) {
  var json = {};
  var source = obj.source;
  var sourceid = obj.sourceId;
  var artistid = obj.artistId || '';
  var albumid = obj.albumId || '';
  var mvpayinfo = '';
  try {
    mvpayinfo = obj.mvpayinfo && encodeURIComponent(JSON.stringify(obj.mvpayinfo)) || '';
  } catch (e) {
    mvpayinfo = '';
  }
  var focussourceid = source === 21 ? getValue(sourceid, 'id') : sourceid;
  if (source === 21 && sourceid.indexOf('?') > -1) {
    sourceid = sourceid + '&from=index';
  }
  extend = obj.extend;
  disname = obj.disName.replace(/(\r|\n)/g, '');
  disname = checkSpecialChar(disname, 'disname');
  titlename = disname;
  titlename = checkSpecialChar(titlename, 'titlename');
  var name = obj.name.replace(/(\r|\n)/g, '');
  name = checkSpecialChar(name, 'name');
  if (source === 2) source = 1;
  /*
  * 焦点图播放乱码处理，将编码这一步去掉，后台返回数据已经编码一次
  * @author 邢祥超 [20190820]
  * */
  // if (source == 7) sourceid = encodeURIComponent(sourceid);
  nodeid = obj.nodeId;
  if (nodeid === '') nodeid = 0;
  var other = '';
  if (source === 8 || source === 12) {
    other = '|psrc=首页->焦点图->|from=index';
  }
  other += '|csrc=曲库->首页->焦点图->' + name;
  var click = commonClickString(new Node(source, sourceid, name, nodeid, extend, other, 'index', artistid, albumid, mvpayinfo));
  var indexnum = obj.indexnum;
  var classname = '';
  if (indexnum === 0) {
    classname = 'current';
  }
  var pic = obj.pic2017;
  if (!pic) {
    pic = 'img/def800.jpg';
  }
  if (pic !== '') {
    pic = changeImgDomain(pic);
  }
  json = {
    'source': source,
    'sourceid': sourceid,
    'focussourceid': focussourceid,
    'classname': classname,
    'name': name,
    'titlename': titlename,
    'click': click,
    'indexnum': indexnum,
    'pic': pic,
    'itemClassName': obj.itemClassName,
    'labelText': getStringKey(extend, 'LABEL_TXT'),
    'labelColor': getStringKey(extend, 'LABEL_COLOR'),
    'labelShow': cornerMark(getStringKey(extend, 'LABEL_SHOW'))
  };
  return json;
}

// 配置角标
function cornerMark(data) {
  var styles = '';
  if (data !== '1') {
    styles = 'none';
  } else {
    styles = 'inline-block';
  }
  return styles;
}

// 焦点图相关方法
var focustimer = null;
var focusindex = 0;

function initJDT() {
  checkJDTPlay(0);
  // 进入页面自动开始定时器
  clearInterval(focustimer);
  focustimer = setInterval(nextimg, 5000);
  $('.focusList_1 img').load(function() {
    setFocus();
    $('#focus_btn').css('display', 'block');
    setIframeHeight();
  });
  bindJDT();
}

// 下一张
function nextimg() {
  $('#focusplay').hide();
  var $item = $('.focusList li');
  focusClassArr.push(focusClassArr[0]);
  focusClassArr.shift();
  $item.each(function(i, e) {
    $(e).removeClass().addClass(focusClassArr[i]);
  });
  setFocus();
  focusindex++;
  if (focusindex > $item.length - 1) {
    focusindex = 0;
  }
  setCurBtn();
}

// 上一张
function previmg() {
  $('#focusplay').hide();
  var $item = $('.focusList li');
  focusClassArr.unshift(focusClassArr[focusClassArr.length - 1]);
  focusClassArr.pop();
  $item.each(function(i, e) {
    $(e).removeClass().addClass(focusClassArr[i]);
  });
  setFocus();
  focusindex--;
  if (focusindex < 0) {
    focusindex = $item.length - 1;
  }
  setCurBtn();
}

// 设置焦点图的容器高度，各个焦点图的位置
function setFocus() {
  var $w_focus = $('#w_focus');
  var $focusList_1 = $('.focusList_1');
  var $focusList_2 = $('.focusList_2');
  var $focusList_last = $('.focusList_last');
  var $focusItem = $('.focusList li');
  var focusList_1Wdith = $focusList_1.width();
  var focusList_1Height = $focusList_1.height();
  var middlePos = ($w_focus.width() - focusList_1Wdith) / 2;
  var middlePosPersent = middlePos / focusList_1Wdith;
  $w_focus.css('height', focusList_1Height);
  $focusItem.css('transform', `translate3d(${middlePosPersent * $focusList_last.width()}px,${0.066 * $focusList_last.height()}px,0) scale(0.885)`);
  $focusList_last.css('transform', `translate3d(0,${0.066 * $focusList_last.height()}px,0) scale(0.885)`);
  $focusList_1.css('transform', `translate3d(${middlePosPersent * $focusList_1.width()}px,0,0) scale(1)`);
  $focusList_2.css('transform', `translate3d(${(middlePosPersent * 100 * 2 + 5) / 100 * $focusList_2.width()}px,${0.066 * $focusList_last.height()}px,0) scale(0.885)`);
  $('.focus .prev,.focus .next').css('bottom', (focusList_1Height * 0.885 - 26) / 2 + 'px');
  $('#focusplay').css('left', middlePos + focusList_1Wdith - 40 + 'px');
}

// 焦点图绑定事件
function bindJDT() {
  // 点击class为focusList_last的元素触发上一张的函数
  $('.focusList_last,.focus .prev').die('click').live('click', function() {
    previmg();
    return false;
  });
  // 点击class为focusList_2的元素触发下一张的函数
  $('.focusList_2,.focus .next').die('click').live('click', function() {
    nextimg();
    return false;
  });
  // 到中间才可以点击
  $('.focusList_1').die('transitionend').live('transitionend', function() {
    var $this = $(this);
    var $focusplay = $('#focusplay');
    $this.addClass('allowClick');
    checkJDTPlay($this.index());
    if ($focusplay.hasClass('isEnter')) $focusplay.show();
  });
  // 焦点图跳转
  $('.focusList li a').die('click').live('click', function() {
    var parent = $(this).parent();
    if (parent.hasClass('allowClick')) {
      eval('(' + parent.attr('data-click') + ')');
    }
  });
  // 鼠标移入w_focus时清除定时器
  $('#focusImg').mouseenter(function(e) {
    clearInterval(focustimer);
    $(this).find('i').show();
    $('#focusplay').addClass('isEnter').show();
    e.stopPropagation();
  });
  $('#focusImg').mouseleave(function() {
    clearInterval(focustimer);
    focustimer = setInterval(nextimg, 5000);
    $(this).find('i').hide();
    $('#focusplay').removeClass('isEnter').hide();
  });
  // 通过底下按钮点击切换
  $('#focus_btn a').mouseenter(function() {
    clearInterval(focustimer);
    var $focusListItem = $('.focusList li');
    var myindex = $(this).index();
    var curIndex = myindex - focusindex;
    if (curIndex === 0) {
      return;
    } else if (curIndex > 0) {
      var newarr = focusClassArr.splice(0, curIndex);
      focusClassArr = $.merge(focusClassArr, newarr);
      $focusListItem.each(function(i, e) {
        $(e).removeClass().addClass(focusClassArr[i]);
      });
    } else if (curIndex < 0) {
      focusClassArr.reverse();
      var oldarr = focusClassArr.splice(0, -curIndex);
      focusClassArr = $.merge(focusClassArr, oldarr);
      focusClassArr.reverse();
      $focusListItem.each(function(i, e) {
        $(e).removeClass().addClass(focusClassArr[i]);
      });
    }
    focusindex = myindex;
    setFocus();
    setCurBtn();
  });
  $('#focus_btn a').mouseleave(function() {
    clearInterval(focustimer);
    focustimer = setInterval(nextimg, 5000);
  });
}

// 改变底下按钮的背景色
function setCurBtn() {
  $('#focus_btn a').eq(focusindex).addClass('current').siblings().removeClass('current').attr('style', '');
  setFocusExposureLog();
}

// 检测焦点图是否可播放
function checkJDTPlay(index) {
  var firstsource = $('.focusList li a').eq(index).attr('data-source');
  var firstsourceid = $('.focusList li a').eq(index).attr('data-sourceid');
  var firstname = $('.focusList li a').eq(index).attr('data-name');
  var $focusplay = $('#focusplay');
  if (firstsource == 1 || firstsource == 4 || firstsource == 8 || firstsource == 12 || firstsource == 13 || firstsource == 21) {
    $focusplay.html('<a data-source="' + firstsource + '" data-sourceid="' + firstsourceid + '" data-name="' + firstname + '" href="javascript:;" hidefocus></a>');
    $focusplay.attr('title', '直接播放');
    if ($focusplay.hasClass('isEnter')) $focusplay.show();
  } else {
    $focusplay.html('').attr('title', '');
  }
}

// 首页焦点图全部播放
var dataCsrc = '';

function focusPlay() {
  var source = $('#focusplay a').attr('data-source');
  var sourceid = $('#focusplay a').attr('data-sourceid');
  var name = $('#focusplay a').attr('data-name');
  var url = '';
  realTimeLog('USRCLK', `TYPE:PCFOCUS_CLICK|SOURCEID:${sourceid}`); // 首页焦点图点击日志
  if (source === '21') {
    iPlayPSRC = '首页->焦点图->精选->' + name;
  } else {
    iPlayPSRC = '首页->焦点图->' + name;
  }
  dataCsrc = '曲库->首页->焦点图->' + name;
  if (source === '21') {
    dataCsrc = '曲库->首页->焦点图->精选->' + name + '精选集';
    $.getScript(album_url + 'album/mbox/commhd?flag=1&id=' + sourceid + '&pn=0&rn=' + iplaynum + '&callback=playZhuanTiMusic');
  } else if (source === '1') {
    url = 'http://kbangserver.kuwo.cn/ksong.s?from=pc&fmt=json&type=bang&data=content&id=' + sourceid + '&callback=playBangMusic&pn=0&rn=' + iplaynum;
    $.getScript(getChargeURL(url));
  } else if (source === '4') {
    url = search_url + 'r.s?stype=artist2music&artistid=' + sourceid + '&pn=0&rn=' + iplaynum + '&newver=1&callback=playArtistMusic';
    $.getScript(getChargeURL(url));
  } else if (source === '8' || source === '12') {
    url = 'http://nplserver.kuwo.cn/pl.svc?op=getlistinfo&pid=' + sourceid + '&pn=0&rn=' + iplaynum + '&encode=utf-8&keyset=pl2012&identity=kuwo&pcmp4=1&newver=1&callback=playGeDanMusic';
    $.getScript(getChargeURL(url));
  } else if (source === '13') {
    url = search_url + 'r.s?stype=albuminfo&albumid=' + sourceid + '&callback=playAlbumMusic&alflac=1&newver=1';
    $.getScript(getChargeURL(url));
  }
}

// 焦点图相关结束
function onSkinChange(skinConfig) {
  $skinConfig = skinConfig;
  $('#focus_btn a').attr('style', '');
}

function setIframeHeight() {
  var bannerIframe = window.parent.document.getElementById('banner_iframe');
  bannerIframe.height = $('body').height();
}

/*
 * @Author: xiangchao.xing
 * @Date: 2022-05-06
 * @Description: 大广告焦点图相关事件
*/
// 大焦点图显示，暂停定时器
function bigBannerShow(val) {
  clearInterval(focustimer);
  if (val) {
    focustimer = setInterval(nextimg, 5000);
    setFocusExposureLog();
  }
}

// 设置跳转参数
function setBigBannerClickObj(obj) {
  var source = obj.source;
  var sourceid = obj.sourceId;
  var mvpayinfo = '';
  var other = '';
  var name = obj.name.replace(/(\r|\n)/g, '');
  var nodeid = obj.nodeId;

  if (nodeid === '') nodeid = 0;

  try {
    mvpayinfo = obj.mvpayinfo && encodeURIComponent(JSON.stringify(obj.mvpayinfo)) || '';
  } catch (e) {
    mvpayinfo = '';
  }

  if (source === 2) source = 1;
  if (source === 21 && sourceid.indexOf('?') > -1) {
    sourceid = sourceid + '&from=index';
  }

  if (source === 8 || source === 12) other = '|psrc=首页->焦点图->|from=index';

  other += '|csrc=曲库->首页->焦点图->' + name;

  bigBannerClickObj = {
    source: source,
    sourceid: sourceid,
    name: checkSpecialChar(name, 'name'),
    nodeid: nodeid,
    extend: obj.extend,
    other: other,
    from: 'index',
    artistid: obj.artistId || '',
    albumid: obj.albumId || '',
    mvpayinfo: mvpayinfo,
    showBanner: obj.showBanner || 0
  };
}

// 设置大焦点图点击事件
function bigBannerClick() {
  commonClick(bigBannerClickObj);
}

// 焦点图轮播日志
function setFocusExposureLog() {
  var exposureItem = $('.focusList_1 a');
  var sourceId = exposureItem.attr('data-sourceid');
  if (logSourceIds.indexOf(sourceId) === -1) {
    logSourceIds.push(sourceId)
    realTimeLog('USRCLK', `TYPE:PCFOCUS_SHOW|SOURCEID:${sourceId}`); // 首页焦点图露出日志
  }
}
